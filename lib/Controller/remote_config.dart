import 'package:firebase_remote_config/firebase_remote_config.dart';

Future<RemoteConfig> setupRemoteConfig() async {
    try {
      final RemoteConfig remoteConfig = await RemoteConfig.instance;

      remoteConfig.setConfigSettings(RemoteConfigSettings(debugMode: false));
      remoteConfig.setDefaults(<String, dynamic>{
        'welcome': 'default welcome',
        'hello': 'default hello',
      });
      await remoteConfig.notifyListeners();
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      return remoteConfig;
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      print(exception);
    } catch (exception) {
      print(exception);
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }
  }