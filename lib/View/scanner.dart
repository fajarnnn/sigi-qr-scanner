import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
// import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:qrcodescanner/Controller/ad_manager.dart';
import 'package:qrcodescanner/View/launcher.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class QRScanner extends StatefulWidget {
  final version;
  QRScanner({Key key, @required this.version}) : super(key: key);
  @override
  _QRScannerState createState() => _QRScannerState();
}

class _QRScannerState extends State<QRScanner> {
  Uint8List bytes = Uint8List(0);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _inputController;
  TextEditingController _outputController;
  InterstitialAd _intersitialAd;
  bool _isInterstitialAdReady;
  @override
  initState() {
    super.initState();
    _isInterstitialAdReady = false;
    this._inputController = new TextEditingController();
    this._outputController = new TextEditingController();
  }

  @override
  void dispose() {
    _intersitialAd?.dispose();
    super.dispose();
  }

  void _loadIntersitialAd() {
    _intersitialAd = InterstitialAd(
      adUnitId: AdManager.interstitialAdUnitId,
      listener: _onInterstitialAdEvent,
    );
    _intersitialAd..load();
    _intersitialAd..show();
  }

  void _onInterstitialAdEvent(MobileAdEvent event) {
    switch (event) {
      case MobileAdEvent.loaded:
        setState(() {
          _isInterstitialAdReady = false;
        });
        print("Sukses");
        break;
      case MobileAdEvent.failedToLoad:
        print('Failed to load an interstitial ad');
        setState(() {
          _isInterstitialAdReady = false;
          _scan();
        });
        break;
      case MobileAdEvent.closed:
        setState(() {
          _isInterstitialAdReady = false;
          _scan();
        });
        break;
      default:
      // setState(() {
      //   _scan();
      // });
      // do nothing
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[300],
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30.0, right: 20.0),
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "Version: " + widget.version,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                ),
                Image(image: AssetImage("assets/images/QrSigi-Logo-qr.png")),
                SizedBox(height: 20),
                TextField(
                  controller: this._outputController,
                  readOnly: true,
                  maxLines: 2,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.wrap_text),
                    helperText:
                        'The barcode or qrcode you scan will be displayed in this area.',
                    hintText:
                        'The barcode or qrcode you scan will be displayed in this area.',
                    hintStyle: TextStyle(fontSize: 15),
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 7, vertical: 15),
                  ),
                ),
                SizedBox(height: 20),
                this._buttonGroup(),
                this._buttonGroup2(),
              ],
            ),
          );
        },
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () => _scanBytes(),
      //   tooltip: 'Take a Photo',
      //   child: const Icon(Icons.camera_alt),
      // ),
    );
  }

  Widget _qrCodeWidget(Uint8List bytes, BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Card(
        elevation: 6,
        child: Column(
          children: <Widget>[
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Icon(Icons.verified_user, size: 18, color: Colors.green),
                  Text('  Generate Qrcode', style: TextStyle(fontSize: 15)),
                  Spacer(),
                  Icon(Icons.more_vert, size: 18, color: Colors.black54),
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 9),
              decoration: BoxDecoration(
                color: Colors.black12,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4), topRight: Radius.circular(4)),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(left: 40, right: 40, top: 30, bottom: 10),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 190,
                    child: bytes.isEmpty
                        ? Center(
                            child: Text('Empty code ... ',
                                style: TextStyle(color: Colors.black38)),
                          )
                        : Image.memory(bytes),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 7, left: 25, right: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Expanded(
                          flex: 5,
                          child: GestureDetector(
                            child: Text(
                              'remove',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.blue),
                              textAlign: TextAlign.left,
                            ),
                            onTap: () =>
                                this.setState(() => this.bytes = Uint8List(0)),
                          ),
                        ),
                        Text('|',
                            style:
                                TextStyle(fontSize: 15, color: Colors.black26)),
                        Expanded(
                          flex: 5,
                          child: GestureDetector(
                            onTap: () async {
                              final success =
                                  await ImageGallerySaver.saveImage(this.bytes);
                              SnackBar snackBar;
                              if (success) {
                                snackBar = new SnackBar(
                                    content:
                                        new Text('Successful Preservation!'));
                                Scaffold.of(context).showSnackBar(snackBar);
                              } else {
                                snackBar = new SnackBar(
                                    content: new Text('Save failed!'));
                              }
                            },
                            child: Text(
                              'save',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.blue),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Divider(height: 2, color: Colors.black26),
            Container(
              child: Row(
                children: <Widget>[
                  Icon(Icons.history, size: 16, color: Colors.black38),
                  Text('  Generate History',
                      style: TextStyle(fontSize: 14, color: Colors.black38)),
                  Spacer(),
                  Icon(Icons.chevron_right, size: 16, color: Colors.black38),
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 9),
            )
          ],
        ),
      ),
    );
  }

  Widget _buttonGroup() {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: SizedBox(
            height: 120,
            child: InkWell(
              onTap: () {
                if (!_isInterstitialAdReady) {
                  setState(() {
                    _isInterstitialAdReady = true;
                    _loadIntersitialAd();
                  });
                }
              },
              child: Card(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: !_isInterstitialAdReady
                          ? Image.asset('assets/images/scanner.png')
                          : SizedBox(
                              width: 70, child: LinearProgressIndicator()),
                    ),
                    Divider(height: 20),
                    Expanded(flex: 1, child: Text("Scan")),
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: SizedBox(
            height: 120,
            child: InkWell(
              onTap: () {
                Scaffold.of(context).showSnackBar(new SnackBar(
                    backgroundColor: Colors.blueAccent,
                    content: new Text(
                      "Premium User Only",
                      textAlign: TextAlign.center,
                    )));
              },
              child: Card(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Image.asset('assets/images/albums.png'),
                    ),
                    Divider(height: 20),
                    Expanded(flex: 1, child: Text("Scan Photo")),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buttonGroup2() {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: SizedBox(
            height: 80,
            child: InkWell(
              onTap: () async {
                var phone = "6281295370837";
                var whatsappUrl = "whatsapp://send?phone=$phone";
                await canLaunch(whatsappUrl)
                    ? launch(whatsappUrl)
                    : print("number Invalid");
              },
              child: Card(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Icon(FontAwesomeIcons.whatsapp),
                    ),
                    Divider(height: 20),
                    Expanded(flex: 1, child: Text("Whatsapp")),
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: SizedBox(
            height: 80,
            child: InkWell(
              onTap: () {
                launch("tel://081295370837");
              },
              child: Card(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Icon(Icons.phone),
                    ),
                    Divider(height: 20),
                    Expanded(flex: 1, child: Text("Call Us")),
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: SizedBox(
            height: 80,
            child: InkWell(
              onTap: () {
                _launchURL('ssigisystems@gmail.com', 'QR Scanner',
                    'Hello Sigisystems');
              },
              child: Card(
                child: Column(
                  children: <Widget>[
                    Expanded(flex: 2, child: Icon(Icons.email)),
                    Divider(height: 20),
                    Expanded(flex: 1, child: Text("Email Us")),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future _scan() async {
    try {
      String barcode = await scanner.scan();

      String link;
      if (!barcode.contains("http") && barcode.contains(".com")) {
        link = "http://" + barcode;
      } else {
        link = barcode;
      }

      if (await canLaunch(link)) {
        launch(link);
        // Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) {
        //   return new QRLauncher();
        // }));
      } else {
        this._outputController.text = barcode;
      }
    } on Exception catch (_) {
      Scaffold.of(context).showSnackBar(new SnackBar(
          backgroundColor: Colors.blueAccent,
          content: new Text(
            "Terjadi Kesalahan Silahkan Scan Ulang",
            textAlign: TextAlign.center,
          )));
    }
  }

  Future _scanPhoto() async {
    String barcode = await scanner.scanPhoto();

    String link;
    if (!barcode.contains("http") && barcode.contains(".com")) {
      link = "http://" + barcode;
    } else {
      link = barcode;
    }
    if (await canLaunch(link)) {
      launch(link);
    } else {
      this._outputController.text = barcode;
    }
  }

  Future _scanPath(String path) async {
    String barcode = await scanner.scanPath(path);

    String link;
    if (!barcode.contains("http") && barcode.contains(".com")) {
      link = "http://" + barcode;
    } else {
      link = barcode;
    }
    if (await canLaunch(link)) {
      launch(link);
    } else {
      this._outputController.text = barcode;
    }
  }

  Future _scanBytes() async {
    File file = await ImagePicker.pickImage(source: ImageSource.camera);
    Uint8List bytes = file.readAsBytesSync();
    String barcode = await scanner.scanBytes(bytes);

    String link;
    if (!barcode.contains("http") && barcode.contains(".com")) {
      link = "http://" + barcode;
    } else {
      link = barcode;
    }
    if (await canLaunch(link)) {
      launch(link);
    } else {
      this._outputController.text = barcode;
    }
  }

  Future _generateBarCode(String inputCode) async {
    Uint8List result = await scanner.generateBarCode(inputCode);
    this.setState(() => this.bytes = result);
  }
}

Future<void> _launchURL(String s, String t, String u) async {
  var url = 'mailto:$s?subject=$t&body=$u';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
