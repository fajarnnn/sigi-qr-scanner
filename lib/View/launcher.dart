import 'dart:async';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:qrcodescanner/Controller/ad_manager.dart';
import 'package:qrcodescanner/Controller/remote_config.dart';
import 'package:qrcodescanner/View/scanner.dart';
import 'package:url_launcher/url_launcher.dart';

class QRLauncher extends StatefulWidget {
  QRLauncher({Key key}) : super(key: key);

  @override
  QRLauncherState createState() => QRLauncherState();
}

class QRLauncherState extends State<QRLauncher> {
  Future<void> _initAdMob() {
    return FirebaseAdMob.instance.initialize(appId: AdManager.appId);
  }

  PackageInfo _packageInfo;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _packageInfo = PackageInfo(
      appName: 'Unknown',
      packageName: 'Unknown',
      version: 'Unknown',
      buildNumber: 'Unk own',
    );
    _initAdMob();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/QrSigi-Logo.png"),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Powered By :",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Image(
                  image: AssetImage("assets/images/sigisystems.png"),
                  height: 130,
                  width: 200,
                  fit: BoxFit.contain,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  navigationPage() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
    RemoteConfig remoteConfig;
    await setupRemoteConfig().then((value) {
      remoteConfig = value;
    });
    String v_code = remoteConfig.getString("version_code");
    int now = int.parse(_packageInfo.buildNumber);
    int then = int.parse(v_code);
    if (now < then) {
      CupertinoAlertDialog cupa = CupertinoAlertDialog(
        title: Text("Update Available"),
        content: Padding(
          padding: const EdgeInsets.only(top:18.0),
          child: Text("Versi Terbaru Sigi QR SCANNER Sudah Rilis, Mohon Update!", ),
        ),
        actions: [
          FlatButton(
            onPressed: () {
              launch(
                  "https://play.google.com/store/apps/details?id=com.sigisystems.qrcodescanner");
            },
            child: Text("OK"),
          ),
        ],
      );
      showDialog(context: context,
        builder: (BuildContext context){
          return cupa;
        }
      );
    } else {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) {
        return new QRScanner(version: _packageInfo.version,);
      }));
    }
  }
}
